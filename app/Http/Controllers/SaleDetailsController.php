<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\SaleDetail;

class SaleDetailsController extends Controller
{
    public function index(){

    }
    public function create(){
        $products = Product::all();
        return view('sale_details.create',compact('products'));
    }

    public function store(Request $request){
        $user = SaleDetail::create([
            'sale_id' => $request->sale_id,
            'product_id' => $request->product_id,
            'qty' => $request->qty,
        ]);
        return redirect()->back()->with('success','Create Successfully!');
    }
}
