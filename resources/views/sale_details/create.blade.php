<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
        <style>
            .fields{
                padding: 30px;
            }
        </style>
    </head>
    <body>
        @if (session()->has('success'))
        <div class="flex justify-content-end px-2 py-2">
        <div class="alert alert-info" align="center">
            <p class="text-lg">{{ session('success') }}</p>
            </div>
        </div>
        @endif
        <form action="{{route('saledetails.store')}}" class="d-flex justify-content-center fields" method="post">
            @csrf
            @method('POST')
            <div class="card col-lg-6">
                <div class="fields">
                    <h3 class="d-flex justify-content-center">Sale</h3>
                    <div class="mb-3">
                        <label class="form-label">Product</label>
                        <select class="form-select" aria-label="Default select example" name="product_id">
                            <option value="" selected>select product</option>
                            @foreach ($products as $product)
                            <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                          </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">sale</label>
                        <input type="text" class="form-control" name="sale_id">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Quantity</label>
                        <input type="text" class="form-control" name="qty">
                    </div>
                    <button type="submit" class="btn btn-primary">save</button>
                    <a type="button" href="{{ url()->previous() }}" class="btn btn-secondary">Back</a>
                </div>
            </div>
          </form>
    </body>
</html>