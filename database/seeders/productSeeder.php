<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'tomato',
            'category_id' => '1',
            'unit_price' => '2',
            'stock_balance' => '100',
        ]);
        DB::table('products')->insert([
            'name' => 'cabbage',
            'category_id' => '1',
            'unit_price' => '2',
            'stock_balance' => '100',
        ]);
        DB::table('products')->insert([
            'name' => 'Eggplant',
            'category_id' => '1',
            'unit_price' => '2',
            'stock_balance' => '100',
        ]);
        DB::table('products')->insert([
            'name' => 'beef',
            'category_id' => '2',
            'unit_price' => '2',
            'stock_balance' => '100',
        ]);
        DB::table('products')->insert([
            'name' => 'Pork',
            'category_id' => '2',
            'unit_price' => '2',
            'stock_balance' => '100',
        ]);
    }
}
