<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class saleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sales')->insert([
            'invoice_number' => '1',
            'total_price' => '10',
        ]);
        DB::table('sales')->insert([
            'invoice_number' => '2',
            'total_price' => '40',
        ]);
        DB::table('sales')->insert([
            'invoice_number' => '3',
            'total_price' => '60',
        ]);
    }
}
